using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using mvc_crud.Models.DB;

#nullable disable

namespace mvc_crud.Data
{
    public partial class appContext : DbContext
    {
        public DbSet<Carro> Carro { get; set; }
        public DbSet<Contato> Contato { get; set; }
        public DbSet<Produtos> Produtos { get; set; }


        
        
        public appContext(DbContextOptions<appContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=app.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);



    }
}
